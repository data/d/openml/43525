# OpenML dataset: NFL-Team-Stats-2002-2019-(ESPN)

https://www.openml.org/d/43525

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

All data is scraped from ESPN's Team Stats page for each game. Seasons include all 256 regular season games plus 11 playoff games, with the exception of 3 games that are missing from ESPN's site:

DALWAS 12-30-2007
CARPIT 12-23-2010
TBATL 1-1-2012

Any errors or quirks in ESPN's data will be present in this dataset. For example, redzone conversions are missing prior to the 2006-07 season.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43525) of an [OpenML dataset](https://www.openml.org/d/43525). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43525/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43525/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43525/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

